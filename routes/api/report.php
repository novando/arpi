<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Artisan;
use App\Http\Controllers\ManifestController;
use App\Http\Controllers\ReportController;

Route::get('manifest/{id}', [ManifestController::class, 'getByVenue'])->middleware('auth:sanctum');
Route::get('manifest-recap/venue/{company_id}', [ReportController::class, 'recapSalesByVenue'])->middleware('auth:sanctum');
Route::get('manifest-recap/combine/{company_id}', [ReportController::class, 'recapSalesCombine'])->middleware('auth:sanctum');
Route::get('manifest-recap/ticket/{company_id}', [ReportController::class, 'recapSalesByTicket'])->middleware('auth:sanctum');
Route::get('manifest-installment/{company_id}', [ReportController::class, 'recapSalesInstallment'])->middleware('auth:sanctum');
Route::get('ticket-recap/{company_id}', [ReportController::class, 'recapTicket'])->middleware('auth:sanctum');
Route::get('ticket-premium-recap/{company_id}', [ReportController::class, 'recapTicketPremium'])->middleware('auth:sanctum');