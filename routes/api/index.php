<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Artisan;
use App\Http\Controllers\UserController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Protected route
Route::prefix('user')
  ->middleware('auth:sanctum')
  ->group(function() {
    require 'user.php';
  });
Route::prefix('role')
  ->middleware('auth:sanctum')  
  ->group(function() {
    require 'role.php';
  });
  Route::prefix('venue')
  ->middleware('auth:sanctum')  
  ->group(function() {
    require 'venue.php';
  });
  
// Unprotected route
Route::prefix('package')->group(function() {
  require 'package.php';
});
Route::prefix('variant')->group(function() {
  require 'variant.php';
});
Route::prefix('company')->group(function() {
  require 'company.php';
});
Route::prefix('pax')->group(function() {
  require 'pax.php';
});
Route::prefix('auth')->group(function() {
  require 'auth.php';
});
Route::prefix('ticket')->group(function() {
  require 'ticket.php';
});
Route::prefix('report')->group(function() {
  require 'report.php';
});
Route::prefix('download')->group(function() {
  require 'download.php';
});
Route::prefix('analytics')->group(function() {
  require 'analytics.php';
});

// Public route
Route::prefix('public')->group(function() {
  require 'public/package.php';
});