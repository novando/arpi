<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Artisan;
use App\Http\Controllers\RoleController;

Route::get('/', [RoleController::class, 'get']);
Route::get('/{id}', [RoleController::class, 'post']);
Route::put('/{id}', [RoleController::class, 'put']);
Route::delete('/{id}', [RoleController::class, 'delete']);
Route::post('/add', [RoleController::class, 'post']);
