<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Artisan;
use App\Http\Controllers\PaxController;

Route::get('/', [PaxController::class, 'get']);
Route::get('/{id}', [PaxController::class, 'post']);
Route::put('/{id}', [PaxController::class, 'put']);
Route::delete('/{id}', [PaxController::class, 'delete']);
Route::post('/add', [PaxController::class, 'post']);
