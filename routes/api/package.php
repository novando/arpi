<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Artisan;
use App\Http\Controllers\PackageController;

Route::get('/', [PackageController::class, 'get'])->middleware('auth:sanctum');
Route::post('/', [PackageController::class, 'post'])->middleware('auth:sanctum');
Route::get('/{id}', [PackageController::class, 'getDetail'])->middleware('auth:sanctum');
Route::delete('/{id}', [PackageController::class, 'delete'])->middleware('auth:sanctum');
Route::put('/{id}', [PackageController::class, 'put'])->middleware('auth:sanctum');
