<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Artisan;
use App\Http\Controllers\VariantController;

Route::get('/', [VariantController::class, 'get'])->middleware('auth:sanctum');
Route::post('/', [VariantController::class, 'post'])->middleware('auth:sanctum');
Route::get('/{id}', [VariantController::class, 'post'])->middleware('auth:sanctum');
Route::put('/{id}', [VariantController::class, 'put'])->middleware('auth:sanctum');
Route::delete('/{id}', [VariantController::class, 'delete'])->middleware('auth:sanctum');
Route::get('/package/{id}', [VariantController::class, 'getPackage'])->middleware('auth:sanctum');
