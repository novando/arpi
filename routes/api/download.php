<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Artisan;
use App\Http\Controllers\DownloadController;

Route::get('manifest/{id}', [DownloadController::class, 'downloadByVenue'])->middleware('auth:sanctum');
Route::get('manifest-recap/venue/{company_id}', [DownloadController::class, 'downloadSalesByVenue'])->middleware('auth:sanctum');
Route::get('manifest-recap/ticket/{company_id}', [DownloadController::class, 'downloadSalesByTicket'])->middleware('auth:sanctum');
Route::get('manifest-recap/combine/{company_id}', [DownloadController::class, 'downloadSalesCombine'])->middleware('auth:sanctum');
Route::get('manifest-installment/{company_id}', [DownloadController::class, 'downloadSalesInstallment'])->middleware('auth:sanctum');
Route::get('ticket-recap/{company_id}', [DownloadController::class, 'downloadRecapTicket'])->middleware('auth:sanctum');
Route::get('ticket-premium-recap/{company_id}', [DownloadController::class, 'downloadRecapTicketPremium'])->middleware('auth:sanctum');