<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Artisan;
use App\Http\Controllers\VenueController;

Route::get('/', [VenueController::class, 'get'])->middleware('auth:sanctum');
Route::get('/company/{id}', [VenueController::class, 'getByCompany'])->middleware('auth:sanctum');
Route::get('/detail/{id}', [VenueController::class, 'getDetail'])->middleware('auth:sanctum');
Route::post('/', [VenueController::class, 'post'])->middleware('auth:sanctum');
Route::put('/{id}', [VenueController::class, 'put'])->middleware('auth:sanctum');
Route::delete('/{id}', [VenueController::class, 'delete'])->middleware('auth:sanctum');
