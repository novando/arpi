<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Artisan;
use App\Http\Controllers\TicketController;

// PROTECTED
Route::get('/', [TicketController::class, 'get'])->middleware('auth:sanctum');
Route::get('/unfinished-order/{company_id}', [TicketController::class, 'getUnfinishedOrdered'])->middleware('auth:sanctum');
Route::post('/', [TicketController::class, 'post'])->middleware('auth:sanctum');
Route::get('/details/{id}', [TicketController::class, 'getDetail'])->middleware('auth:sanctum');
Route::put('/{id}', [TicketController::class, 'put'])->middleware('auth:sanctum');