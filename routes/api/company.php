<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Artisan;
use App\Http\Controllers\CompanyController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/', [CompanyController::class, 'get'])->middleware('auth:sanctum');
Route::get('/mine', [CompanyController::class, 'getMine'])->middleware('auth:sanctum');
Route::get('/one/{id}', [CompanyController::class, 'post'])->middleware('auth:sanctum');
Route::put('/', [CompanyController::class, 'put'])->middleware('auth:sanctum');
Route::delete('/one/{id}', [CompanyController::class, 'delete'])->middleware('auth:sanctum');
Route::post('/', [CompanyController::class, 'post'])->middleware('auth:sanctum');
