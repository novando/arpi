<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Artisan;
use App\Http\Controllers\AnalyticsController;

Route::get('pax/{company_id}', [AnalyticsController::class, 'paxAnalytics'])->middleware('auth:sanctum');
Route::get('sale/{company_id}', [AnalyticsController::class, 'saleAnalytics'])->middleware('auth:sanctum');
Route::get('package/{company_id}', [AnalyticsController::class, 'packageAnalytics'])->middleware('auth:sanctum');
Route::get('venue/{company_id}', [AnalyticsController::class, 'venueAnalytics'])->middleware('auth:sanctum');
Route::get('pax-segment/{company_id}', [AnalyticsController::class, 'paxSegmentAnalytics'])->middleware('auth:sanctum');
Route::get('pax-gender/{company_id}', [AnalyticsController::class, 'paxGenderAnalytics'])->middleware('auth:sanctum');
Route::get('pax-hometown/{company_id}', [AnalyticsController::class, 'paxHometownAnalytics'])->middleware('auth:sanctum');