<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Artisan;
use App\Http\Controllers\PackageController;
use App\Http\Controllers\VariantController;
use App\Http\Controllers\PaxController;
use App\Http\Controllers\TicketController;


Route::get('/package/{company_id}', [PackageController::class, 'getPublic']);
Route::get('/variant/{company_id}/{package_id}', [VariantController::class, 'getPublic']);
Route::post('/pax/add', [PaxController::class, 'postPublic']);
Route::post('/pax/register/{id_tiket}', [PaxController::class, 'registerPublic']);
Route::get('/ticket/{id_ticket}', [TicketController::class, 'getDetailPublic']);