<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PackageSeeder extends Seeder
{
	/**
	 * Seed the application's database.
	 *
	 * @return void
	 */
	public function run()
	{
		DB::table('packages')->insert([
      [
        'name' => 'Pesiar',
        'description' => '',
        'company_id' => 2,
        'created_by' => 'DeafaultSeeder',
        'updated_by' => 'DeafaultSeeder',
        'created_at' => now(),
        'updated_at' => now(),
      ],
      [
        'name' => 'Prewedding',
        'description' => '',
        'company_id' => 2,
        'created_by' => 'DeafaultSeeder',
        'updated_by' => 'DeafaultSeeder',
        'created_at' => now(),
        'updated_at' => now(),
      ],
      [
        'name' => 'Praktikum',
        'description' => '',
        'company_id' => 2,
        'created_by' => 'DeafaultSeeder',
        'updated_by' => 'DeafaultSeeder',
        'created_at' => now(),
        'updated_at' => now(),
      ],
      [
        'name' => 'Perahu Wisata',
        'description' => '',
        'company_id' => 3,
        'created_by' => 'DeafaultSeeder',
        'updated_by' => 'DeafaultSeeder',
        'created_at' => now(),
        'updated_at' => now(),
      ],
		]);
	}
}
