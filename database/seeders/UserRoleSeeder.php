<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UserRoleSeeder extends Seeder
{
	/**
	 * Seed the application's database.
	 *
	 * @return void
	 */
	public function run()
	{
		DB::table('roles')->insert([
      [
        'name' => 'superadmin',
        'created_by' => 'DeafaultSeeder',
        'updated_by' => 'DeafaultSeeder',
        'created_at' => now(),
        'updated_at' => now(),
      ],
		  [
        'name' => 'jasaraharja',
        'created_by' => 'DeafaultSeeder',
        'updated_by' => 'DeafaultSeeder',
        'created_at' => now(),
        'updated_at' => now(),
      ],
		  [
        'name' => 'comapany',
        'created_by' => 'DeafaultSeeder',
        'updated_by' => 'DeafaultSeeder',
        'created_at' => now(),
        'updated_at' => now(),
      ],
		  [
        'name' => 'teller',
        'created_by' => 'DeafaultSeeder',
        'updated_by' => 'DeafaultSeeder',
        'created_at' => now(),
        'updated_at' => now(),
      ],
		]);
	}
}
