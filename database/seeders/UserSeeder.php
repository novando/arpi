<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UserSeeder extends Seeder
{
	/**
	 * Seed the application's database.
	 *
	 * @return void
	 */
	public function run()
	{
		DB::table('users')->insert([
      [
        'name' => 'Superadmin Arutek',
        'email' => 'superadmin@arutek.com',
        'password' => bcrypt('superadmin'),
        'company_id' => '1',
        'role_id' => '1',
        'created_by' => 'DeafaultSeeder',
        'updated_by' => 'DeafaultSeeder',
        'created_at' => now(),
        'updated_at' => now(),
      ],
      [
        'name' => 'Admin Arutek',
        'email' => 'admin@arutek.com',
        'password' => bcrypt('arutek'),
        'company_id' => '1',
        'role_id' => '3',
        'created_by' => 'DeafaultSeeder',
        'updated_by' => 'DeafaultSeeder',
        'created_at' => now(),
        'updated_at' => now(),
      ],
      [
        'name' => 'Admin Bukit Cinta',
        'email' => 'admin@bukitcinta.com',
        'password' => bcrypt('bukitcinta'),
        'company_id' => '2',
        'role_id' => '3',
        'created_by' => 'DeafaultSeeder',
        'updated_by' => 'DeafaultSeeder',
        'created_at' => now(),
        'updated_at' => now(),
      ],
      [
        'name' => 'Teller Bukit Cinta',
        'email' => 'teller@bukitcinta.com',
        'password' => bcrypt('bukitcinta'),
        'company_id' => '2',
        'role_id' => '4',
        'created_by' => 'DeafaultSeeder',
        'updated_by' => 'DeafaultSeeder',
        'created_at' => now(),
        'updated_at' => now(),
      ],
      [
        'name' => 'Admin Kampoeng Rawa',
        'email' => 'admin@kampoengrawa.com',
        'password' => bcrypt('kampoengrawa'),
        'company_id' => '3',
        'role_id' => '3',
        'created_by' => 'DeafaultSeeder',
        'updated_by' => 'DeafaultSeeder',
        'created_at' => now(),
        'updated_at' => now(),
      ],
      [
        'name' => 'Teller Kampoeng Rawa',
        'email' => 'teller@kampoengrawa.com',
        'password' => bcrypt('kampoengrawa'),
        'company_id' => '3',
        'role_id' => '4',
        'created_by' => 'DeafaultSeeder',
        'updated_by' => 'DeafaultSeeder',
        'created_at' => now(),
        'updated_at' => now(),
      ],
		]);
	}
}
