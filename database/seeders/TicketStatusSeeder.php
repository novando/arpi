<?php

namespace Database\Seeders;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Seeder;

class TicketStatusSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public static function run()
	{
		DB::table('ticket_statuses')->insert([
			[
				'name' => 'WAIT_PAYMENT',
				'created_by' => 'DefaultSeeder',
				'updated_by' => 'DefaultSeeder',
				'created_at' => now(),
				'updated_at' => now(),
			],
			[
				'name' => 'PAID',
				'created_by' => 'DefaultSeeder',
				'updated_by' => 'DefaultSeeder',
				'created_at' => now(),
				'updated_at' => now(),
			],
			[
				'name' => 'CANCELED',
				'created_by' => 'DefaultSeeder',
				'updated_by' => 'DefaultSeeder',
				'created_at' => now(),
				'updated_at' => now(),
			],
		]);
	}
}
