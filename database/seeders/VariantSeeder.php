<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class VariantSeeder extends Seeder
{
	/**
	 * Seed the application's database.
	 *
	 * @return void
	 */
	public function run()
	{
		DB::table('variants')->insert([
      [
        'name' => 'Keliling Danau 30 Menit',
        'price' => 120000,
        'max_capacity' => 6,
        'duration' => 30,
        'days' => '[0,1,2,3,4,5,6]',
        'package_id' => 1,
        'created_by' => 'DeafaultSeeder',
        'updated_by' => 'DeafaultSeeder',
        'created_at' => now(),
        'updated_at' => now(),
      ],
      [
        'name' => '30 Menit',
        'price' => 120000,
        'max_capacity' => 4,
        'duration' => 30,
        'days' => '[0,1,2,3,4,5,6]',
        'package_id' => 2,
        'created_by' => 'DeafaultSeeder',
        'updated_by' => 'DeafaultSeeder',
        'created_at' => now(),
        'updated_at' => now(),
      ],
      [
        'name' => '60 Menit',
        'price' => 240000,
        'max_capacity' => 4,
        'duration' => 60,
        'days' => '[0,1,2,3,4,5,6]',
        'package_id' => 2,
        'created_by' => 'DeafaultSeeder',
        'updated_by' => 'DeafaultSeeder',
        'created_at' => now(),
        'updated_at' => now(),
      ],
      [
        'name' => 'Jam 8-12',
        'price' => 250000,
        'max_capacity' => 4,
        'duration' => 240,
        'days' => '[0,1,2,3,4,5,6]',
        'package_id' => 2,
        'created_by' => 'DeafaultSeeder',
        'updated_by' => 'DeafaultSeeder',
        'created_at' => now(),
        'updated_at' => now(),
      ],
      [
        'name' => 'Jam 8-16',
        'price' => 300000,
        'max_capacity' => 4,
        'duration' => 480,
        'days' => '[0,1,2,3,4,5,6]',
        'package_id' => 2,
        'created_by' => 'DeafaultSeeder',
        'updated_by' => 'DeafaultSeeder',
        'created_at' => now(),
        'updated_at' => now(),
      ],
      [
        'name' => '30 Menit',
        'price' => 120000,
        'max_capacity' => 4,
        'duration' => 30,
        'days' => '[0,1,2,3,4,5,6]',
        'package_id' => 3,
        'created_by' => 'DeafaultSeeder',
        'updated_by' => 'DeafaultSeeder',
        'created_at' => now(),
        'updated_at' => now(),
      ],
      [
        'name' => '60 Menit',
        'price' => 240000,
        'max_capacity' => 4,
        'duration' => 60,
        'days' => '[0,1,2,3,4,5,6]',
        'package_id' => 3,
        'created_by' => 'DeafaultSeeder',
        'updated_by' => 'DeafaultSeeder',
        'created_at' => now(),
        'updated_at' => now(),
      ],
      [
        'name' => 'Jam 8-12',
        'price' => 250000,
        'max_capacity' => 4,
        'duration' => 240,
        'days' => '[0,1,2,3,4,5,6]',
        'package_id' => 3,
        'created_by' => 'DeafaultSeeder',
        'updated_by' => 'DeafaultSeeder',
        'created_at' => now(),
        'updated_at' => now(),
      ],
      [
        'name' => 'Jam 8-16',
        'price' => 300000,
        'max_capacity' => 4,
        'duration' => 480,
        'days' => '[0,1,2,3,4,5,6]',
        'package_id' => 3,
        'created_by' => 'DeafaultSeeder',
        'updated_by' => 'DeafaultSeeder',
        'created_at' => now(),
        'updated_at' => now(),
      ],
      [
        'name' => 'Weekday',
        'price' => 100000,
        'max_capacity' => 8,
        'duration' => 60,
        'days' => '[1,2,3,4,5]',
        'package_id' => 4,
        'created_by' => 'DeafaultSeeder',
        'updated_by' => 'DeafaultSeeder',
        'created_at' => now(),
        'updated_at' => now(),
      ],
      [
        'name' => 'Weekend',
        'price' => 120000,
        'max_capacity' => 8,
        'duration' => 60,
        'days' => '[0,6]',
        'package_id' => 4,
        'created_by' => 'DeafaultSeeder',
        'updated_by' => 'DeafaultSeeder',
        'created_at' => now(),
        'updated_at' => now(),
      ],
		]);
	}
}
