<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Database\Seeders\CompanySeeder;
use Database\Seeders\UserRoleSeeder;
use Database\Seeders\UserSeeder;
use Database\Seeders\PackageSeeder;
use Database\Seeders\VariantSeeder;
use Database\Seeders\TicketStatusSeeder;

class DatabaseSeeder extends Seeder
{
	/**
	 * Seed the application's database.
	 *
	 * @return void
	 */
	public function run()
	{
		$this->call([
			CompanySeeder::class,
			TicketStatusSeeder::class,
			UserRoleSeeder::class,
			UserSeeder::class,
			PackageSeeder::class,
			VariantSeeder::class,
		]);
	}
}
