<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CompanySeeder extends Seeder
{
	public function run()
	{
    DB::table('companies')->insert([
			[
				'name' => 'Arutek',
				'logo' => '',
				'owner' => 'Rizky Novando Priyadi',
				'secretary' => '',
				'finance' => '',
				'province' => 'Papua',
				'city' => 'Kabupaten Merauke',
				'address' => 'Jl. Raya Mandala Spadem no.369B',
				'phone' => '+6285155233290',
				'created_by' => 'DeafaultSeeder',
				'updated_by' => 'DeafaultSeeder',
				'created_at' => now(),
				'updated_at' => now(),
			],
			[
				'name' => 'Perahu Motor Bukit Cinta',
				'logo' => '',
				'owner' => '',
				'secretary' => '',
				'finance' => '',
				'province' => 'Jawa Tengah',
				'city' => 'Kabupaten Semarang',
				'address' => '',
				'phone' => '',
				'created_by' => 'DeafaultSeeder',
				'updated_by' => 'DeafaultSeeder',
				'created_at' => now(),
				'updated_at' => now(),
			],
			[
				'name' => 'Perahu Motor Kampoeng Rawa',
				'logo' => '',
				'owner' => 'H. Rachkmad Kristianto Adi',
				'secretary' => 'Vincentius Kristiyanto',
				'finance' => 'Purwanto',
				'province' => 'Jawa Tengah',
				'city' => 'Kabupaten Semarang',
				'address' => '',
				'phone' => '',
				'created_by' => 'DeafaultSeeder',
				'updated_by' => 'DeafaultSeeder',
				'created_at' => now(),
				'updated_at' => now(),
			]
		]);
	}
}