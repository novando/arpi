<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('paxes', function (Blueprint $table) {
			$table->id();
			$table->string('name');
			$table->enum('sex', ['male', 'female']);
			$table->unsignedTinyInteger('age');
			$table->string('province');
			$table->string('city');
			$table->string('address')->nullable();
			$table->boolean('registrant')->default(false);
			$table->unsignedBigInteger('ticket_id')->nullable();
			$table->string('created_by')->nullable();
			$table->string('updated_by')->nullable();
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('paxes');
	}
};
