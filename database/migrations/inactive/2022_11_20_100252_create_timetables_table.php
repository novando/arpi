<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Models\Venue;

return new class extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('timetables', function (Blueprint $table) {
			$table->id();
			$table->foreignIdFor(Venue::class)->constrained()->cascadeOnDelete();
			$table->boolean('chartered')->default(false);
			$table->time('scheduled_departure');
			$table->time('scheduled_arrival');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('timetables');
	}
};
