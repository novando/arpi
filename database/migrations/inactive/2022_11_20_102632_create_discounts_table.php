<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('discounts', function (Blueprint $table) {
			$table->id();
			$table->string('voucher');
			$table->unsignedDecimal('percentage', $precision = 5, $scale = 2)->nullable();
			$table->unsignedDecimal('maximal_amount', $precision = 9, $scale = 2)->nullable();
			$table->unsignedDecimal('new_value', $precision = 9, $scale = 2)->nullable();
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('discounts');
	}
};
