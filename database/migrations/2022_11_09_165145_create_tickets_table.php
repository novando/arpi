<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('tickets', function (Blueprint $table) {
			$table->id();
			$table->string('id_ticket')->unique();
			$table->string('phone');
			$table->string('pic')->nullable();
			$table->unsignedDecimal('price', $precision = 9, $scale = 2)->nullable();
			$table->unsignedBigInteger('venue_id')->nullable();
			$table->unsignedBigInteger('variant_id');
			$table->unsignedBigInteger('ticket_status_id')->default(1);
			$table->string('created_by')->nullable();
			$table->string('updated_by')->nullable();
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('tickets');
	}
};
