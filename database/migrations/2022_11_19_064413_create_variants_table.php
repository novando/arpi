<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('variants', function (Blueprint $table) {
			$table->id();
			$table->string('name')->nullable();
			$table->unsignedDecimal('price', $precision = 9, $scale = 2);
			$table->unsignedDecimal('max_capacity', $precision = 3, $scale = 1);
			$table->unsignedInteger('duration');
			$table->json('days')->default('[0,1,2,3,4,5,6]');
			$table->boolean('deleted')->default(false);
			$table->unsignedBigInteger('package_id')->nullable();
			$table->string('created_by')->nullable();
			$table->string('updated_by')->nullable();
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('variants');
	}
};
