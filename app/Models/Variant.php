<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Variant extends Model
{
	use HasFactory;

	protected $fillable = [
		'name',
		'price',
		'duration',
		'max_capacity',
		'package_id',
		'days',
		'deleted',
		'created_by',
		'updated_by',
	];
}
