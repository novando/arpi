<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Venue extends Model
{
	use HasFactory;

	protected $fillable = [
		'name',
		'capacity',
		'owner',
		'company_id',
		'deleted',
		'created_by',
		'updated_by',
	];
	protected $casts = [
		'days' => 'array'
	];
}
