<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
	use HasFactory;

	protected $fillable = [
		'name',
		'logo',
		'owner',
		'secretary',
		'finance',
		'province',
		'city',
		'address',
		'phone',
		'created_by',
		'updated_by',
	];
}
