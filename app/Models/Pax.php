<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Pax extends Model
{
	use HasFactory;

	protected $fillable = [
		'name',
		'sex',
		'age',
		'province',
		'city',
		'address',
		'registrant',
		'ticket_id',
		'created_by',
		'updated_by',
	];
}
