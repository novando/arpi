<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Ticket extends Model
{
	use HasFactory;

	protected $fillable = [
		'id_ticket',
		'variant_id',
		'ticket_status_id',
		'phone',
		'created_by',
		'updated_by',
	];
}
