<?php

namespace App\Helpers;

class Sorter {
  public static function dateSorter ($start, $end) {
    $dateNow = date_create(date('Y-m-d\TH:i:s'));
    return [
      'end' => $end = $end ? $end: $dateNow->format('Y-m-d\TH:i:s'),
      'start' => $start = $start ? $start : $dateNow->modify('-1 month')->format('Y-m-d\TH:i:s')
    ];
  }
}