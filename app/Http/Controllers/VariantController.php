<?php

namespace App\Http\Controllers;

use App\Models\Variant;
use App\Models\Package;
use Illuminate\Http\Request;

class VariantController extends Controller
{
	private function getPackageIds(Request $req) {
		$company_id = $req->user()->company_id;
		$package_ids = Package::where('company_id', $company_id)->get('id');
		if (!isset($package_ids)) return null;
		return $package_ids;
	}

	// Protected API
	public function get(Request $req) {
		$package_ids = getPackageIds($req);
		if (!isset($package_ids)) return response()->json('VARIANT_NOT_FOUND', 404);
		$variants = Variant::where('package_id', $package_ids)->where('deleted', false)->get();
		return response()->json($variants, 200);
	}
	public function getPackage(Request $req, $id) {
		$company_id = $req->user()->company_id;
		$variants = Variant::where('package_id', $id)->where('deleted', false)->get();
		if (!isset($variants)) return response()->json('VARIANT_NOT_FOUND', 404);
		return response()->json($variants, 200);
	}
	public function getDetail(Variant $id) {
		$company = Variant::where('id', $id)->where('deleted', false)->first();
		if (!isset($company)) return response()->json('VARIANT_NOT_FOUND', 404);
		return response()->json($company, 200);
	}
	public function delete(Request $req, $id) {
		$variant = Variant::where('id', $id)->get();
		if (!isset($variant)) return response()->json('VARIANT_NOT_FOUND', 404);
		$query = ['deleted' => true];
		Variant::where('id', $id)->update($query);
		return response()->json(true, 200);
	}
	public function put(Request $req) {
		foreach($req->all() as $item) {
			$temp = [
				'name' => $item['name'],
				'days' => $item['days'],
			];
			$variant = Variant::where('id', $item['id'])->update($temp);
		}
		return response()->json('VARIANTS_UPDATED', 200);
	}
	public static function post(Request $req) {
		$data = $req->validate([
			'name' => 'required',
			'max_capacity' => 'required|numeric',
			'duration' => 'required|numeric',
			'price' => 'required|numeric',
			'package_id' => 'required|numeric',
		]);
		Variant::create($req->all());
		return response()->json('VARIANT_CREATED', 201);
	}

	// Public API
	public function getPublic(Request $req, $company_id, $package_id) {
		$company = Package::where('id', $package_id)->first();
		if ((string)$company['company_id'] !== $company_id) return response()->json('COMPANY_PACKAGE_MISSMATCH', 400);
		$variants = Variant::where('package_id', $package_id)->where('deleted', false)->get();
		return response()->json($variants, 200);
	}
	public function getDetailPublic(Request $req, $company_id, $package_id, $variant_id) {
		$company = Package::where('id', $package_id)->first();
		if ((string)$company['company_id'] !== $company_id) return response()->json('COMPANY_PACKAGE_MISSMATCH', 400);
		$variants = Variant::where('package_id', $package_id)->where('id', $variant_id)->where('deleted', false)->first();
		return response()->json($variants, 200);
	}
}
