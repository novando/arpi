<?php

namespace App\Http\Controllers;

use App\Models\Pax;
use App\Models\Ticket;
use App\Models\Package;
use App\Models\Variant;
use Illuminate\Http\Request;

class PaxController extends Controller
{
	private function codeGenerator () {
		$characters = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < 10; $i++) {
			$randomString .= $characters[rand(0, $charactersLength - 1)];
    }
		return $randomString;
	}
	public function postPublic (Request $req) {
		$data = $req->validate([
			'company_id' => 'required|numeric',
			'package_id' => 'required|numeric',
			'variant_id' => 'required|numeric',
			'phone' => 'required',
		]);
		$package = Package::where('company_id', $req['company_id'])->get();
		if (!isset($package)) return response()->json(404);
		$package_ids = [];
		foreach ($package as $item) {
			array_push($package_ids, $item['id']);
		}
		$variant = Variant::whereIn('package_id', $package_ids)->where('id', $req['variant_id'])->first();
		if (!isset($variant)) return response()->json(400);
		$tickets = Ticket::all();
		$ticket_ids = [];
		foreach ($tickets as $item) {
			array_push($ticket_ids, $item['id_ticket']);
		}
		$identic = true;
		$id_ticket = '';
		while ($identic) {
			$id_ticket = $this->codeGenerator();
			if (!in_array($id_ticket, $ticket_ids)) $identic = false; 
		}
		$new_req = [
			'id_ticket' => $id_ticket,
			'phone' => $req['phone'],
			'variant_id' => $variant['id'],
			'ticket_status_id' => $req['ticket_status_id'] ? $req['ticket_status_id'] : 1,
		];
		$ticket = Ticket::create($new_req);
		foreach ($req['pax'] as $item) {
			$item['ticket_id'] = $ticket['id'];
			Pax::create($item);
		}
		return response()->json($ticket, 200);
	}
	public function registerPublic (Request $req, $id_ticket) {
		$data = $req->validate([
			'phone' => 'required',
			'pax' => 'required|numeric',
		]);
		// TODO: Update phone number in "tickets" table
		foreach ($req['pax'] as $item) {
			$item['ticket_id'] = $id_ticket;
			Pax::create($item);
		}
		return response()->json($ticket, 200);
	}
}
