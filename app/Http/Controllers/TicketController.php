<?php

namespace App\Http\Controllers;

use App\Models\Ticket;
use App\Models\Variant;
use App\Models\Package;
use App\Models\Company;
use App\Models\Pax;
use App\Models\Venue;
use Illuminate\Http\Request;

class TicketController extends Controller
{
	private function getVariantIds (Request $req, $company_id = null) {
		$company_id = $company_id !== null ? $company_id : $req->user()->company_id;
		$packages = Package::where('company_id', $company_id)->get();
		$package_ids = [];
		foreach ($packages as $item) {
			array_push($package_ids, $item['id']);
		}
		if (!isset($package_ids)) return null;
		$variants = Variant::whereIn('package_id', $package_ids)->get();
		if (!isset($variants)) return null;
		$variant_ids = [];
		foreach ($variants as $item) {
			array_push($variant_ids, $item['id']);
		}
		return $variant_ids;
	}

	// Protected API
	public function getUnfinishedOrdered (Request $req, $company_id) {
		$variant_ids = $this->getVariantIds($req, $company_id);
		if (!isset($variant_ids)) return response()->json('TICKET_NOT_FOUND', 404);
		$tickets = Ticket::whereIn('variant_id', $variant_ids)->where('ticket_status_id', 1)->orderBy('id', 'DESC')->get();
		if (!isset($tickets)) return response()->json('TICKET_NOT_FOUND', 404);
		foreach ($tickets as $item) {
			$variant = Variant::where('id', $item['variant_id'])->first();
			$item['variant_name'] = $variant['name'];
			$item['price'] = $variant['price'];
			$package = Package::where('id', $variant['package_id'])->first();
			$item['package_name'] = $package['name'];
		}
		return response()->json($tickets, 200);
	}
	public function get (Request $req) {
		$variant_ids = $this->getVariantIds($req);
		if (!isset($variant_ids)) return response()->json('TICKET_NOT_FOUND', 404);
		$tickets = Ticket::whereIn('variant_id', $variant_ids)->orderBy('id', 'DESC')->get();
		if (!isset($tickets)) return response()->json('TICKET_NOT_FOUND', 404);
		foreach ($tickets as $item) {
			$variant = Variant::where('id', $item['variant_id'])->first();
			$item['variant_name'] = $variant['name'];
			$item['price'] = $variant['price'];
			$package = Package::where('id', $variant['package_id'])->first();
			$item['package_name'] = $package['name'];
		}
		return response()->json($tickets, 200);
	}
	public function getDetail (Request $req, $id) {
		$variant_ids = $this->getVariantIds($req);
		if (!isset($variant_ids)) return response()->json('COMPANY_NOT_FOUND', 404);
		$ticket = Ticket::where('id_ticket', $id)->whereIn('variant_id', $variant_ids)->first();
		if (!isset($ticket)) return response()->json('TICKET_NOT_FOUND', 404);
		$variant = Variant::where('id', $ticket['variant_id'])->first();
		$ticket['price'] = $variant['price'];
		$ticket['pax'] = Pax::where('ticket_id', $ticket['id'])->get();
		return response()->json($ticket, 200);
	}
	public function put (Request $req, $id) {
		$variant_ids = $this->getVariantIds($req);
		if (!isset($variant_ids)) return response()->json('TICKET_NOT_FOUND', 404);
		$data = $req->validate([
			'id_ticket' => 'required|size:10',
			'variant_id' => 'required|numeric',
			'ticket_status_id' => 'required|numeric',
		]);
		$payload = [
			'price' => $req['price'],
			'pic' => $req['pic'],
			'ticket_status_id' => $req['ticket_status_id'],
			'venue_id' => $req['venue_id'],
		];
		$ticket = Ticket::where('id', $id)->whereIn('variant_id', $variant_ids)->update($payload);
		if (!isset($ticket)) return response()->json('TICKET_NOT_FOUND', 404);
		return response()->json($ticket, 200);
	}
	public function post (Request $req) {
		$variant_ids = getVariantIds($req);
		if (!in_array($req['variant_id'], $variant_ids)) {
			return response()->json('VARIANT_ID_NOT_FOUND', 404);
		} else {
			$data = $req->validate([
				'id_ticket' => 'required|size:10',
				'variant_id' => 'required|numeric',
				'ticket_status_id' => 'required|numeric',
			]);
			$res = Ticket::create($req->all());
			return response()->json($res, 201);
		}
	}

	// Public API
	public function getDetailPublic (Request $req, $id_ticket) {
		$ticket = Ticket::where('id_ticket', $id_ticket)->first();
		if (!isset($ticket)) return response()->json('TICKET_NOT_FOUND', 404);
		$variant = Variant::where('id', $ticket['variant_id'])->first();
		$package = Package::where('id', $variant['package_id'])->first();
		if (!isset($package)) return response()->json('PACKAGE_NOT_FOUND', 404);
		$company = Company::where('id', $package['company_id'])->first();
		$venue = Venue::where('id', $ticket['venue_id'])->first();
		$ticket['price'] = $ticket['price'] ? $ticket['price'] : $variant['price'];
		$ticket['name'] = $package['name'] . ' ' . $variant['name'];
		$ticket['company_id'] = $package['company_id'];
		$ticket['company_name'] = $company['name'];
		$ticket['venue_name'] = $venue ? $venue['name'] : '';
		$ticket['pax'] = Pax::where('ticket_id', $ticket['id'])->get();
		return response()->json($ticket, 200);
	}
}
