<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Models\User;

class UserController extends Controller {
	public function register(Request $req) {
		$req['role_id'] = $req['role_id'] ? $req['role_id'] : 4;
		if ($req['password'] !== $req['conf_password']) return response()->json('PASS_MISSMATCH', 400);
		$req['password'] = bcrypt($req['password']);
		$req->validate([
			'name' => 'required',
			'email' => 'required|email',
			'password' => 'required',
			'role_id' => 'required|numeric',
		]);
		User::create($req->all());
		$user = User::where('email', $req['email'])->first();
		$token = $user->createToken($user->email)->plainTextToken;
		return response()->json($token, 200);
	}
	public function login(Request $req) {
		$req->validate([
			'email' => 'required|email',
			'password' => 'required',
		]);
		$user = User::where('email', $req['email'])->first();
		if (!$user || !Hash::check($req['password'], $user->password)) return response()->json('LOGIN_FAILED', 404);
		$user->tokens()->delete(); // Destroy all other token before issued a new one
		$token = $user->createToken($user->email)->plainTextToken;
		return response()->json($token, 200);
	}
	public function me(Request $req) {
		return response()->json($req->user()->role_id, 200);
	}
	public function profile(Request $req) {
		$user = $req->user();
		return response()->json($user, 200);
	}
}
