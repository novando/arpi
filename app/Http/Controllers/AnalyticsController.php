<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Company;
use App\Models\Pax;
use App\Models\Package;
use App\Models\Variant;
use App\Models\Ticket;
use App\Models\Venue;
use App\Helpers\Sorter;
use App\Http\Controllers\ReportController;

class AnalyticsController extends Controller
{
  public function paxAnalytics (Request $req, $company_id) {
    $recap = ReportController::recapTicketHelper($req, $company_id);
    $res = [];
    foreach ($recap as $item) {
      if ($res === []) {
        $res = $item['daily'];
      } else {
        for ($i = 0; $i < count($item['daily']); $i++) {
          $res[$i] += $item['daily'][$i]; 
        }
      }
    }
    return response()->json($res, 200);
  }
  public function saleAnalytics (Request $req, $company_id) {
    $recap = ReportController::recapTicketHelper($req, $company_id);
    $res = [];
    foreach ($recap as $item) {
      if ($res === []) {
        $res = $item['daily_value'];
      } else {
        for ($i = 0; $i < count($item['daily_value']); $i++) {
          $res[$i] += $item['daily_value'][$i]; 
        }
      }
    }
    return response()->json($res, 200);
  }
  public function paxSegmentAnalytics (Request $req, $company_id) {
    $company_id = $company_id;
    $role_id = $req->user()->role_id;
    if ($role_id > 3) return response()->json('ACCESS_DENIED', 403);
    $date = Sorter::dateSorter($req->query('startDate'), $req->query('endDate'));
    $package_ids = Package::where('company_id', $company_id)->pluck('id');
    $variant_ids = Variant::whereIn('package_id', $package_ids)->pluck('id');
    $ticket_ids = Ticket::where('ticket_status_id', 2)->whereIn('variant_id', $variant_ids)->pluck('id');
		if (!isset($ticket_ids)) return response()->json('TICKET_NOT_FOUND', 404);
    $res['baby'] = 0;
    $res['child'] = 0;
    $res['teen'] = 0;
    $res['young'] = 0;
    $res['adult'] = 0;
    $res['senior'] = 0;
    $paxes = Pax::whereIn('ticket_id', $ticket_ids)
    ->where('created_at', '>=', $date['start'])
    ->where('created_at', '<=', $date['end'])
      ->get();
    $res['total'] = count($paxes);
    foreach ($paxes as $pax) {
      $age = $pax['age'];
      if ($age < 6) {
        $res['baby']++;
      } else if ($age >= 6 && $age < 12) {
        $res['child']++;
      } else if ($age >= 12 && $age < 21) {
        $res['teen']++;
      } else if ($age >= 21 && $age < 40) {
        $res['young']++;
      } else if ($age >= 40 && $age < 60) {
        $res['adult']++;
      } else {
        $res['senior']++;
      }
    }
    return response()->json($res, 200);
  }
  public function paxGenderAnalytics (Request $req, $company_id) {
    $company_id = $company_id;
    $role_id = $req->user()->role_id;
    if ($role_id > 3) return response()->json('ACCESS_DENIED', 403);
    $date = Sorter::dateSorter($req->query('startDate'), $req->query('endDate'));
    $package_ids = Package::where('company_id', $company_id)->pluck('id');
    $variant_ids = Variant::whereIn('package_id', $package_ids)->pluck('id');
    $ticket_ids = Ticket::where('ticket_status_id', 2)->whereIn('variant_id', $variant_ids)->pluck('id');
		if (!isset($ticket_ids)) return response()->json('TICKET_NOT_FOUND', 404);
    $res['male'] = Pax::whereIn('ticket_id', $ticket_ids)
      ->where('sex', 'male')
      ->where('created_at', '>=', $date['start'])
      ->where('created_at', '<=', $date['end'])
      ->get()
      ->count();
    $res['female'] = Pax::whereIn('ticket_id', $ticket_ids)
      ->where('sex', 'female')
      ->where('created_at', '>=', $date['start'])
      ->where('created_at', '<=', $date['end'])
      ->get()
      ->count();
    $res['total'] = $res['male'] + $res['female'];
    return response()->json($res, 200);
  }
  public function paxHometownAnalytics (Request $req, $company_id) {
    $company_id = $company_id;
    $role_id = $req->user()->role_id;
    if ($role_id > 3) return response()->json('ACCESS_DENIED', 403);
    $date = Sorter::dateSorter($req->query('startDate'), $req->query('endDate'));
    $package_ids = Package::where('company_id', $company_id)->pluck('id');
    $variant_ids = Variant::whereIn('package_id', $package_ids)->pluck('id');
    $ticket_ids = Ticket::where('ticket_status_id', 2)->whereIn('variant_id', $variant_ids)->pluck('id');
		if (!isset($ticket_ids)) return response()->json('TICKET_NOT_FOUND', 404);
    $addresses = Pax::whereIn('ticket_id', $ticket_ids)
      ->where('created_at', '>=', $date['start'])
      ->where('created_at', '<=', $date['end'])
      ->pluck('city');
    $unique_addresses = [];
    foreach ($addresses as $address) {
      if (in_array($address, $unique_addresses)) {
        continue;
      } else {
        array_push($unique_addresses, $address);
      }
    }
    $res['total'] = 0;
    foreach ($unique_addresses as $address) {
      $res[ucwords($address)] = Pax::whereIn('ticket_id', $ticket_ids)
      ->where('city', $address)
      ->where('created_at', '>=', $date['start'])
      ->where('created_at', '<=', $date['end'])
      ->get()
      ->count();
      $res['total'] += $res[ucwords($address)]; 
    }
    return response()->json($res, 200);
  }
  public function packageAnalytics (Request $req, $company_id) {
    $company_id = $company_id;
    $role_id = $req->user()->role_id;
    if ($role_id > 3) return response()->json('ACCESS_DENIED', 403);
    $date = Sorter::dateSorter($req->query('startDate'), $req->query('endDate'));
    $package_ids = Package::where('company_id', $company_id)->pluck('id');
    $variants = Variant::whereIn('package_id', $package_ids)->get();
		if (!isset($variants)) return response()->json('VARIANT_NOT_FOUND', 404);
    $variant_ids = Variant::whereIn('package_id', $package_ids)->pluck('id');
    $ticket_variant_ids = Ticket::where('ticket_status_id', 2)
      ->whereIn('variant_id', $variant_ids)
      ->where('created_at', '>=', $date['start'])
      ->where('created_at', '<=', $date['end'])
      ->pluck('variant_id');
    $temp_variant_ids = [];
    foreach ($ticket_variant_ids as $ticket_variant_id) {
      array_push($temp_variant_ids, $ticket_variant_id);
    }
    $sum_variant_ids = array_count_values($temp_variant_ids);
    foreach ($variants as $variant) {
      $package = Package::where ('id', $variant['package_id'])->first();
      $name = $package['name'] . ' - ' . $variant['name'];
      $res[$name] = 0;
    }
    foreach ($variants as $variant) {
      $package = Package::where ('id', $variant['package_id'])->first();
      $name = $package['name'] . ' - ' . $variant['name'];
      $res[$name] += isset($sum_variant_ids[$variant['id']]) ? $sum_variant_ids[$variant['id']] : 0;
    }
    if (empty($res)) return response()->json([], 200);
    arsort($res);
    array_splice($res, 3);
    return response()->json($res, 200);
  }
  public function venueAnalytics (Request $req, $company_id) {
    $company_id = $company_id;
    $role_id = $req->user()->role_id;
    if ($role_id > 3) return response()->json('ACCESS_DENIED', 403);
    $date = Sorter::dateSorter($req->query('startDate'), $req->query('endDate'));
    $package_ids = Package::where('company_id', $company_id)->pluck('id');
    $variants = Variant::whereIn('package_id', $package_ids)->get();
		if (!isset($variants)) return response()->json('VARIANT_NOT_FOUND', 404);
    $variant_ids = Variant::whereIn('package_id', $package_ids)->pluck('id');
    $venue_ids = Ticket::where('ticket_status_id', 2)
      ->whereIn('variant_id', $variant_ids)
      ->where('created_at', '>=', $date['start'])
      ->where('created_at', '<=', $date['end'])
      ->pluck('venue_id');
    $venues = Venue::whereIn('id', $venue_ids)->get();
    $temp_venue_ids = [];
    foreach ($venue_ids as $venue_id) {
      array_push($temp_venue_ids, $venue_id);
    }
    $sum_venue_ids = array_count_values($temp_venue_ids);
    foreach ($venues as $venue) {
      $res[$venue['name']] = $sum_venue_ids[$venue['id']];
    }
    if (empty($res)) return response()->json([], 200);
    arsort($res);
    array_splice($res, 3);
    return response()->json($res, 200);
  }
}
