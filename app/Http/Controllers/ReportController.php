<?php

namespace App\Http\Controllers;

use App\Models\Venue;
use App\Models\Package;
use App\Models\Ticket;
use App\Models\Variant;
use App\Models\Pax;
use App\Helpers\Sorter;
use Illuminate\Http\Request;

class ReportController extends Controller
{
  // Helpers
  public static function recapSalesByVenueHelper (Request $req, $company_id) {
    $role_id = $req->user()->role_id;
    if ($role_id > 3) return response()->json('ACCESS_DENIED', 403);
		$date = Sorter::dateSorter($req->query('startDate'), $req->query('endDate'));
		$venues = Venue::where('company_id', $company_id)->get();
		if (!isset($venues)) return response()->json('VENUE_NOT_FOUND', 404);
    $res = [];
    foreach ($venues as $venue) {
      $temp['name'] = $venue['name'];
      $tickets = Ticket::where('venue_id', $venue['id'])
        ->where('ticket_status_id', 2)
        ->where('created_at', '>=', $date['start'])
        ->where('created_at', '<=', $date['end'])
        ->get();
      $temp['total_ticket'] = count($tickets);
      $temp['total_pax'] = 0;
      $temp['total_sale'] = 0;
      foreach ($tickets as $ticket) {
        $temp['total_pax'] += Pax::where('ticket_id', $ticket['id'])->count();
        $temp['total_sale'] += $ticket['price'];
      }
      if ($temp['total_sale'] > 0) array_push($res, $temp);
    }
    return $res;
  }
  public static function recapSalesCombineHelper (Request $req, $company_id) {
    $role_id = $req->user()->role_id;
    if ($role_id > 3) return response()->json('ACCESS_DENIED HERE', 403);
		$date = Sorter::dateSorter($req->query('startDate'), $req->query('endDate'));
		$venues = Venue::where('company_id', $company_id)->get();
		if (!isset($venues)) return response()->json('VENUE_NOT_FOUND', 404);
		$packages = Package::where('company_id', $company_id)->get();
		if (!isset($packages)) return response()->json('PACKAGE_NOT_FOUND', 404);
    $res = [];
    foreach ($venues as $venue) {
      $temp['venue'] = $venue['name'];
      foreach ($packages as $package) {
        $variants = Variant::where('package_id', $package['id'])->get();
        if (!isset($variants)) continue;
        foreach ($variants as $variant) {
          $tickets = Ticket::where('variant_id', $variant['id'])
            ->where('venue_id', $venue['id'])
            ->where('ticket_status_id', 2)
            ->where('created_at', '>=', $date['start'])
            ->where('created_at', '<=', $date['end'])
            ->get();
          if (!isset($tickets)) continue;
          $temp['package'] = $package['name'] . ' ' . $variant['name'];
          $temp['price'] = $variant['price'];
          $temp['total_ticket'] = count($tickets);
          $temp['total_pax'] = 0;
          $temp['total_sale'] = 0;
          foreach ($tickets as $ticket) {
            $temp['total_pax'] += Pax::where('ticket_id', $ticket['id'])->count();
            $temp['total_sale'] += $ticket['price'];
          }
          if ($temp['total_sale'] > 0) array_push($res, $temp);
        }
      }
    }
    return $res;
  }
  public static function recapSalesByTicketHelper (Request $req, $company_id) {
    $role_id = $req->user()->role_id;
    if ($role_id > 3) return response()->json('ACCESS_DENIED', 403);
		$date = Sorter::dateSorter($req->query('startDate'), $req->query('endDate'));
		$packages = Package::where('company_id', $company_id)->get();
		if (!isset($packages)) return response()->json('PACKAGE_NOT_FOUND', 404);
    $res = [];
    foreach ($packages as $package) {
      $variants = Variant::where('package_id', $package['id'])->get();
      if (!isset($variants)) continue;
      foreach ($variants as $variant) {
        $tickets = Ticket::where('variant_id', $variant['id'])
          ->where('ticket_status_id', 2)
          ->where('created_at', '>=', $date['start'])
          ->where('created_at', '<=', $date['end'])
          ->get();
        if (!isset($tickets)) continue;
        $temp['name'] = $package['name'] . ' ' . $variant['name'];
        $temp['price'] = $variant['price'];
        $temp['total_ticket'] = count($tickets);
        $temp['total_pax'] = 0;
        $temp['total_sale'] = 0;
        foreach ($tickets as $ticket) {
          $temp['total_pax'] += Pax::where('ticket_id', $ticket['id'])->count();
          $temp['total_sale'] += $ticket['price'];
        }
        if ($temp['total_sale'] > 0) array_push($res, $temp);
      }
    }
    return $res;
  }
  public static function recapSalesInstallmentHelper (Request $req, $company_id) {
    $role_id = $req->user()->role_id;
		if ($role_id > 2) return response()->json('ACCESS_DENIED', 403);
		$date = Sorter::dateSorter($req->query('startDate'), $req->query('endDate'));
		$venues = Venue::where('company_id', $company_id)->get();
		if (!isset($venues)) return response()->json('VENUE_NOT_FOUND', 404);
    $res = [];
    foreach ($venues as $venue) {
      $tickets = Ticket::where('venue_id', $venue['id'])
        ->where('created_at', '>=', $date['start'])
        ->where('created_at', '<=', $date['end'])
        ->where('ticket_status_id', 2)
        ->get();
      $temp['name'] = $venue['name'];
      $temp['total_ticket'] = count($tickets);
      $temp['total_pax'] = 0;
      $temp['total_sale'] = 0;
			$temp['total_ticket_premium'] = 0; 
      foreach ($tickets as $ticket) {
        $temp['total_pax'] += Pax::where('ticket_id', $ticket['id'])->count();
				$variant = Variant::where('id', $ticket['variant_id'])->first();
				$temp['total_ticket_premium'] += $variant['max_capacity'] * 200;
        $temp['total_sale'] += $ticket['price'];
      }
			$temp['total_pax_premium'] = $temp['total_pax'] * 200;
      if ($temp['total_sale'] > 0) array_push($res, $temp);
    }
    return $res;
  }
	public static function recapTicketHelper (Request $req, $company_id) {
    $role_id = $req->user()->role_id;
    if ($role_id > 3) return response()->json('ACCESS_DENIED', 403);
    $date = Sorter::dateSorter($req->query('startDate'), $req->query('endDate'));
		$packages = Package::where('company_id', $company_id)->get();
		if (!isset($packages)) return response()->json('PACKAGE_NOT_FOUND', 404);
    $start_date = date_create($date['start']);
    $end_date = date_create($date['end']);
    $queryVenueId = $req->query('venueId', '0');
    $res = [];
    foreach ($packages as $package) {
      $variants = Variant::where('package_id', $package['id'])->get();
      if (!isset($variants)) continue;
      foreach ($variants as $variant) {
        $temp['name'] = $package['name'] . ' ' . $variant['name'];
        $temp['price'] = $variant['price'];
        $temp['total_pax'] = 0;
        $temp['total_sale'] = 0;
        $temp['daily'] = [];
        $temp['daily_value'] = [];
        while ($start_date < $end_date) {
          if ($queryVenueId !== '0') {
            $tickets = Ticket::where('variant_id', $variant['id'])
            ->where('venue_id', $queryVenueId)
            ->where('ticket_status_id', 2)
            ->where('created_at', '>=', $start_date->format('Y-m-d\TH:i:s'))
            ->where('created_at', '<=', $start_date->modify('+1 day')->format('Y-m-d\TH:i:s'))
            ->get();
          } else {
            $tickets = Ticket::where('variant_id', $variant['id'])
            ->where('ticket_status_id', 2)
            ->where('created_at', '>=', $start_date->format('Y-m-d\TH:i:s'))
            ->where('created_at', '<=', $start_date->modify('+1 day')->format('Y-m-d\TH:i:s'))
            ->get();
          }
          $daily = 0;
          $daily_value = 0;
          if (isset($tickets)) {
            foreach ($tickets as $ticket) {
              $daily_value += $ticket['price'];
              $temp['total_sale'] += $ticket['price'];
              $daily += Pax::where('ticket_id', $ticket['id'])->count();
            }
          }
          $temp['total_pax'] += $daily;
          array_push($temp['daily_value'], $daily_value);
          array_push($temp['daily'], $daily);
        }
        $start_date = date_create($date['start']);
        if ($temp['total_pax'] > 0) array_push($res, $temp);
      }
    }
    return $res;
	}
	public static function recapTicketPremiumHelper (Request $req, $company_id) {
    $role_id = $req->user()->role_id;
    if ($role_id > 2) return response()->json('ACCESS_DENIED', 403);
    $date = Sorter::dateSorter($req->query('startDate'), $req->query('endDate'));
		$packages = Package::where('company_id', $company_id)->get();
		if (!isset($packages)) return response()->json('PACKAGE_NOT_FOUND', 404);
    $start_date = date_create($date['start']);
    $end_date = date_create($date['end']);
    $queryVenueId = $req->query('venueId', '0');
    $res = [];
    foreach ($packages as $package) {
      $variants = Variant::where('package_id', $package['id'])->get();
      if (!isset($variants)) continue;
      foreach ($variants as $variant) {
        $temp['name'] = $package['name'] . ' ' . $variant['name'];
        $temp['price'] = $variant['price'];
        $temp['tariff'] = $variant['max_capacity'] * 200;
        $temp['total_pax'] = 0;
        $temp['total_sale'] = 0;
        $temp['daily'] = [];
        while ($start_date < $end_date) {
          if ($queryVenueId !== '0') {
            $tickets = Ticket::where('variant_id', $variant['id'])
            ->where('venue_id', $queryVenueId)
            ->where('ticket_status_id', 2)
            ->where('created_at', '>=', $start_date->format('Y-m-d\TH:i:s'))
            ->where('created_at', '<=', $start_date->modify('+1 day')->format('Y-m-d\TH:i:s'))
            ->get();
          } else {
            $tickets = Ticket::where('variant_id', $variant['id'])
            ->where('ticket_status_id', 2)
            ->where('created_at', '>=', $start_date->format('Y-m-d\TH:i:s'))
            ->where('created_at', '<=', $start_date->modify('+1 day')->format('Y-m-d\TH:i:s'))
            ->get();
          }
          $daily = 0;
          if (isset($tickets)) {
            foreach ($tickets as $ticket) {
              $temp['total_sale'] += $ticket['price'];
              $daily += Pax::where('ticket_id', $ticket['id'])->count();
            }
          }
          $temp['total_pax'] += $daily;
          array_push($temp['daily'], $daily);
        }
        $start_date = date_create($date['start']);
        $temp['total_tariff'] = $temp['total_pax'] * 200;
        if ($temp['total_sale'] > 0) array_push($res, $temp);
      }
    }
    return $res;
	}

  // Controllers
	public function recapSalesByVenue (Request $req, $company_id) {
    return response()->json($this->recapSalesByVenueHelper($req, $company_id), 200);
	}
  public function recapSalesCombine (Request $req, $company_id) {
    return response()->json($this->recapSalesCombineHelper($req, $company_id), 200);
  }
	public function recapSalesByTicket (Request $req, $company_id) {
    return response()->json($this->recapSalesByTicketHelper($req, $company_id), 200);
	}
	public function recapSalesInstallment (Request $req, $company_id) {
    return response()->json($this->recapSalesInstallmentHelper($req, $company_id), 200);
	}
  public function recapTicket (Request $req, $company_id) {
    return response()->json($this->recapTicketHelper($req, $company_id), 200);
  }
  public function recapTicketPremium (Request $req, $company_id) {
    return response()->json($this->recapTicketPremiumHelper($req, $company_id), 200);
  }
}
