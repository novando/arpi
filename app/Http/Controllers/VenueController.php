<?php

namespace App\Http\Controllers;

use App\Models\Venue;
use Illuminate\Http\Request;

class VenueController extends Controller
{
	public function get(Request $req) {
		if ($req->user()->role_id <= 2) {
			$venue = Venue::where('deleted', false)->get();
			return response()->json($venue, 200);
		} else {
			$company_id = $req->user()->company_id;
			$venue = Venue::where('company_id', $company_id)->where('deleted', false)->get();
			return response()->json($venue, 200);
		}
	}
	public function getByCompany(Request $req, $company_id) {
		$venue = Venue::where('company_id', $company_id)->where('deleted', false)->get();
		return response()->json($venue, 200);
	}
	public function getDetail(Venue $id) {
		$company = Venue::where('id', $id)->first();
		if (!isset($company)) return response()->json('VENUE_NOT_FOUND', 404);
		return response()->json($company, 200);
	}
	public function put(Request $req, $id) {
		$data = $req->validate([
			'name' => 'required',
			'owner' => 'required',
			'capacity' => 'required|numeric',
			'company_id' => 'required|numeric',
		]);
		$request = [
			'name' => $req['name'],
			'owner' => $req['owner'],
			'capacity' => $req['capacity']
		];
		$company = Venue::where('id', $id)->where('deleted', false)->update($request);
		if (!isset($company)) return response()->json('VENUE_NOT_FOUND', 404);
		return response()->json($company, 200);
	}
	public function post(Request $req) {
		$req['company_id'] = $req->user()->company_id;
		$data = $req->validate([
			'name' => 'required',
			'owner' => 'required',
			'capacity' => 'required|numeric',
			'company_id' => 'required|numeric',
		]);
		Venue::create($req->all());
		return response()->json('VENUE_CREATED', 201);
	}
	public function delete(Request $req, $id) {
		$venue = Venue::where('id', $id)->get();
		if (!isset($venue)) return response()->json('VENUE_NOT_FOUND', 404);
		$query = ['deleted' => true];
		Venue::where('id', $id)->update($query);
		return response()->json(true, 200);
	}
}
