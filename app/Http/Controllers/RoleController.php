<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Role;

class RoleController extends Controller
{
	public function get (Request $req) {
		return Role::all();
	}
	public function getDetail (Role $id) {
		$company = Role::where('id', $id)->first();
		if (!isset($company)) return response()->json('ROLE_NOT_FOUND', 404);
		return response()->json($company, 200);
	}
	public function delete (Role $id) {
		$company = Role::where('id', $id)->delete();
		if (!isset($company)) return response()->json('ROLE_NOT_FOUND', 404);
		return response()->json($company, 200);
	}
	public function put (Request $req, Role $id) {
		$data = $req->validate([
			'name' => 'required',
		]);
		$company = Role::where('id', $id)->update($req);
		if (!isset($company)) return response()->json('ROLE_NOT_FOUND', 404);
		return response()->json($company, 200);
	}
	public function post (Request $req) {
		$data = $req->validate([
			'name' => 'required',
		]);
		Role::create($req->all());
		return response()->json('ROLE_CREATED', 201);
	}
}
