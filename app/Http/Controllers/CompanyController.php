<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Company;

class CompanyController extends Controller
{
	// superadmin only
	public function delete(Request $req, $id) {
		if ($req->user()->role_id > 1) return response()->json('ACCESS_DENIED', 403);
		$company = Company::where('id', $id)->delete();
		if (!isset($company)) return response()->json('COMPANY_NOT_FOUND', 404);
		return response()->json($company, 200);
	}

	// minimum access level 2
	public function post(Request $req) {
		if ($req->user()->role_id > 2) return response()->json('ACCESS_DENIED', 403);
		$data = $req->validate([
			'name' => 'required',
			'owner' => 'required',
			'province' => 'required',
			'city' => 'required',
			'phone' => 'required',
		]);
		Company::create($req->all());
		return response()->json('COMPANY_CREATED', 201);
	}
	public function get(Request $req) {
		if ($req->user()->role_id > 2) return response()->json('ACCESS_DENIED', 403);
		return response()->json(Company::all(), 200);
	}
	public function getDetail(Requset $req, $id) {
		if ($req->user()->role_id > 2) return response()->json('ACCESS_DENIED', 403);
		$company = Company::where('id', $id)->first();
		if (!isset($company)) return response()->json('COMPANY_NOT_FOUND', 404);
		return response()->json($company, 200);
	}

	// minimum access level 3
	public function getMine (Request $req) {
		if ($req->user()->role_id > 3) return response()->json('ACCESS_DENIED', 403);
		$company = Company::where('id', $req->user()->company_id)->first();
		return response()->json($company, 200);
	}
	public function put(Request $req) {
		if ($req->user()->role_id > 3) return response()->json('ACCESS_DENIED', 403);
		$id = $req->user()->company_id;
		if (!isset($req['logo'])) $req['logo'] = '';
		$request = [
			'name' => $req['name'],
			'owner' => $req['owner'] ? $req['owner'] : '',
			'secretary' => $req['secretary'] ? $req['secretary'] : '',
			'finance' => $req['finance'] ? $req['finance'] : '',
			'province' => $req['province'] ? $req['province'] : '',
			'city' => $req['city'] ? $req['city'] : '',
			'phone' => $req['phone'] ? $req['phone'] : '',
		];
		error_log($req);
		$company = Company::where('id', $id)->update($request);
		if (!isset($company)) return response()->json('COMPANY_NOT_FOUND', 404);
		return response()->json($company, 200);
	}

	// Public API
}
