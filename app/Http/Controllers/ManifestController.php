<?php

namespace App\Http\Controllers;

use App\Models\Venue;
use App\Models\Package;
use App\Models\Ticket;
use App\Models\Variant;
use App\Models\Pax;
use App\Helpers\Sorter;
use Illuminate\Http\Request;

class ManifestController extends Controller
{
	public static function getByVenueHelper (Request $req, $id) {
		$date = Sorter::dateSorter($req->query('startDate'), $req->query('endDate'));
		if ($req->user()->role_id <= 2) {
			$venue = Venue::where('id', $id)->first();
			if (!isset($venue)) return response()->json('VENUE_NOT_FOUND', 404);
			$company_id = $venue['company_id'];
		} else {
			$company_id = $req->user()->company_id;
			$venue = Venue::where('company_id', $company_id)->where('id', $id)->first();
			if (!isset($venue)) return response()->json('VENUE_NOT_FOUND', 404);
		}
		$packages = Package::where('company_id', $company_id)->get();
		if (!isset($packages)) return response()->json('PACKAGE_NOT_FOUND', 404);
		$package_ids = [];
		foreach ($packages as $item) {
			array_push($package_ids, $item['id']);
		}
		$variants = Variant::whereIn('package_id', $package_ids)->get();
		if (!isset($variants)) return response()->json('VARIANTS_NOT_FOUND', 404);
		$variant_ids = [];
		foreach ($variants as $item) {
			array_push($variant_ids, $item['id']);
		}

		if (!isset($variant_ids)) return response()->json('COMPANY_NOT_FOUND', 404);
		$tickets = Ticket::where('venue_id', $id)
			->where('ticket_status_id', 2)
			->where('created_at', '>=', $date['start'])
			->where('created_at', '<=', $date['end'])
			// ->whereBetween('created_at', [$date['start'], $date['end']])
			->orderBy('created_at', 'ASC')
			->get();
		if (!isset($tickets)) return response()->json('TICKET_NOT_FOUND', 404);
		$res = [];
		foreach ($tickets as $item) {
			$variant = null;
			foreach ($variants as $theVariant) {
				if ($theVariant['id'] === $item['variant_id']) $variant = $theVariant;
			}
			$package_name = '';
			foreach ($packages as $package) {
				if ($package['id'] === $variant['package_id']) $package_name = $package['name'];
			}
			$paxes = Pax::where('ticket_id', $item['id'])->orderBy('id', 'ASC')->get();
			foreach ($paxes as $pax) {
				$temp['pic'] = $item['pic'];
				$temp['departure'] = $item['created_at'];
				$temp['id_ticket'] = $item['id_ticket'];
				$temp['pax_name'] = $pax['name'];
				$temp['pax_sex'] = $pax['sex'];
				$temp['pax_age'] = $pax['age'];
				$temp['pax_address'] = $pax['city'];
				$temp['pax_phone'] = $pax['registrant'] ? $item['phone'] : '';
				$temp['ticket'] = $pax['registrant'] ? $package_name . ' ' . $variant['name'] : '';
				$temp['price'] = $pax['registrant'] ? $item['price'] : null;
				array_push($res, $temp);
			}
		}
		return $res;
	}
	public function getByVenue (Request $req, $id) {
		$res = $this->getByVenueHelper($req, $id);
		return response()->json($res, 200);
	}
}
