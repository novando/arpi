<?php

namespace App\Http\Controllers;

use App\Models\Package;
use App\Models\Variant;
use App\Models\Company;
use Illuminate\Http\Request;
use App\Http\Controllers\VariantController;

class PackageController extends Controller
{
	// Protected API
	public function get(Request $req) {
		$company_id = $req->user()->company_id;
		$packages = Package::where('company_id', $company_id)->where('deleted', false)->get();
		$packages->transform(function ($item) {
			$variant = Variant::where('package_id', $item['id'])->where('deleted', false)->first();
			$item['price'] = $variant ? $variant['price'] : 0;
			$item['max_capacity'] = $variant ? $variant['max_capacity'] : 0;
			$item['duration'] = $variant ? $variant['duration'] : 0;
			return $item;
		});
		return response()->json($packages, 200);
	}
	public function getDetail(Request $req, $id) {
		$company_id = $req->user()->company_id;
		$company = Package::where('id', $id)->where('company_id', $company_id)->where('deleted', false)->first();
		if (!isset($company)) return response()->json('PACKAGE_NOT_FOUND', 404);
		return response()->json($company, 200);
	}
	public function put(Request $req, $id) {
		$req['company_id'] = $req->user()->company_id;
		$data = $req->validate([
			'name' => 'required',
			'company_id' => 'required|numeric',
		]);
		$newReq = [
			'name' => $req['name'],
			'description'=> $req['description'],
		];
		$company = Package::where('id', $id)->where('company_id', $req['company_id'])->update($newReq);
		if (!isset($company)) return response()->json('PACKAGE_NOT_FOUND', 404);
		return response()->json($company, 200);
	}
	public function delete(Request $req, $id) {
		$package = Package::where('id', $id)->get();
		if (!isset($package)) return response()->json('PACKAGE_NOT_FOUND', 404);
		$query = ['deleted' => true];
		Package::where('id', $id)->update($query);
		return response()->json(true, 200);
	}
	public function post(Request $req) {
		$req['company_id'] = $req->user()->company_id;
		$req['price'] = (double) $req['price'];
		$req['max_capacity'] = (int) $req['max_capacity'];
		$req['duration'] = (int) $req['duration'];
		$data = $req->validate([
			'name' => 'required',
			'max_capacity' => 'required|numeric',
			'duration' => 'required|numeric',
			'price' => 'required|numeric',
			'company_id' => 'required|numeric',
		]);
		$package = Package::create($req->all());
		$req['name'] = 'default';
		$req['package_id'] = $package['id'];
		VariantController::post($req);
		return response()->json($package, 201);
	}

	// Public API
	public function getPublic(Request $req, $company_id) {
		$res['package_data'] = Package::where('company_id', $company_id)->where('deleted', false)->get();
		$company = Company::where('id', $company_id)->first();
		$res['company_name'] = $company['name'];
		return response()->json($res, 200);
	}
}
