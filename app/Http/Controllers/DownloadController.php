<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Company;
use App\Models\Venue;
use App\Helpers\Sorter;
use App\Http\Controllers\ReportController;
use App\Http\Controllers\ManifestController;

class DownloadController extends Controller
{
  protected static function downloadSettings ($res, $texts, $bottom_line, $footers = []) {
    $headers = [
      "Content-type"        => "text/csv",
      "Content-Disposition" => "attachment; filename=export.csv",
      "Pragma"              => "no-cache",
      "Cache-Control"       => "must-revalidate, post-check=0, pre-check=0",
      "Expires"             => "0"
    ];
    $callback = function() use($res, $texts, $bottom_line, $footers) {
      $file = fopen('php://output', 'w');
      foreach ($texts as $text) {
        fwrite($file, $text);
      }
      $counter = 0;
      foreach ($res as $line) {
        $counter += 1;
        fwrite($file, "$counter;");
        fputcsv($file, $line, ';');
      }
      fwrite($file, "$bottom_line\n");
      if (count($footers) > 0) {
        foreach ($footers as $text) {
          fwrite($file, $text);
        }
      }
      $print_date = date_create(date('Y-m-d\TH:i:s'))->modify('+7 hours')->format('d/m/Y H:i:s');
      fwrite($file, "\ndicetak pada tanggal $print_date WIB");
      fclose($file);
    };
    return [$callback, $headers];
  }
  public function downloadByVenue (Request $req, $id) {
    $company_id = $req->user()->company_id;
    $company = Company::where('id', $company_id)->first();
    $venue = Venue::where('id', $id)->first();
    $date = Sorter::dateSorter($req->query('startDate'), $req->query('endDate'));
    $venue_name = $venue['name'];
    $company_name = $company['name'];
    $company_owner = $company['owner'];
    $company_finance = $company['finance'];
    $company_city = $company['city'];
    $end_date = date_create($date['end'])->modify('+7 hours')->format('d/m/Y');
    $start_date = date_create($date['start'])->modify('+7 hours')->format('d/m/Y');
    $now_date = date_create(date('Y-m-d\TH:i:s'))->modify('+7 hours')->format('d F Y');
    $texts = [
      "Dokumen;: Detail Manifest Penumpang;\n",
      "Paguyuban;: $company_name;\n",
      "Kapal;: $venue_name;\n",
      "Periode;: $start_date s.d. $end_date;\n\n",
      "No;Keberangkatan;Nomor e-Tiket;Nama Penumpang;Gender;Usia;Alamat;No.Telp;Jenis Tiket;Harga Tiket;\n"
    ];
    $footers = [
      "\n\n",
      ";;;;;;$company_city, $now_date;",
      "\n\n\n\n",
      ";$company_owner;;;;;$company_finance;\n",
      ";Ketua Paguyuban;;;;;Bendahara;\n",
    ];
    $res = ManifestController::getByVenueHelper($req, $id);
    $total_price = 0;
    foreach ($res as $item) {
      $total_price += $item['price'];
    }
    $bottom_line = ";Jumlah;;;;;;;;$total_price;";
    $settings = $this->downloadSettings($res, $texts, $bottom_line, $footers);
    return response()->streamDownload($settings[0], 'SalesByVenue.csv', $settings[1]);
  }
  public function downloadSalesByVenue (Request $req, $company_id) {
    $role_id = $req->user()->role_id;
    $user_company_id = $req->user()->company_id;
    if ($role_id > 2 && $user_company_id != $company_id ) return response()->json('ACCESS_DENIED', 403);
    $company = Company::where('id', $company_id)->first();
    $date = Sorter::dateSorter($req->query('startDate'), $req->query('endDate'));
    $company_name = $company['name'];
    $company_owner = $company['owner'];
    $company_finance = $company['finance'];
    $company_city = $company['city'];
    $end_date = date_create($date['end'])->modify('+7 hours')->format('d/m/Y');
    $start_date = date_create($date['start'])->modify('+7 hours')->format('d/m/Y');
    $now_date = date_create(date('Y-m-d\TH:i:s'))->modify('+7 hours')->format('d F Y');
    $texts = [
      "Dokumen;: Rekap Penjualan Tiket;\n",
      "Paguyuban;: $company_name;\n",
      "Berdasarkan;: Jenis Perahu;\n",
      "Periode;: $start_date s.d. $end_date;\n\n",
      "No;Nama Kapal;Jumlah Produksi;Jumlah Penumpang;Penerimaan;\n"
    ];
    $footers = [
      "\n\n",
      ";;;$company_city, $now_date;",
      "\n\n\n\n",
      ";$company_owner;;$company_finance;\n",
      ";Ketua Paguyuban;;Bendahara;\n",
    ];
    $res = ReportController::recapSalesByVenueHelper($req, $company_id);
    $total_sale = 0;
    $total_pax = 0;
    $total_ticket = 0;
    foreach ($res as $item) {
      $total_sale += $item['total_sale'];
      $total_pax += $item['total_pax'];
      $total_ticket += $item['total_ticket'];
    }
    $bottom_line = ";Jumlah;$total_ticket;$total_pax;$total_sale;";
    $settings = $this->downloadSettings($res, $texts, $bottom_line, $footers);
    return response()->streamDownload($settings[0], 'SalesByVenue.csv', $settings[1]);
  }
  public function downloadSalesByTicket (Request $req, $company_id) {
    $role_id = $req->user()->role_id;
    $user_company_id = $req->user()->company_id;
    if ($role_id > 2 && $user_company_id != $company_id ) return response()->json('ACCESS_DENIED', 403);
    $company = Company::where('id', $company_id)->first();
    $date = Sorter::dateSorter($req->query('startDate'), $req->query('endDate'));
    $company_name = $company['name'];
    $company_owner = $company['owner'];
    $company_finance = $company['finance'];
    $company_city = $company['city'];
    $end_date = date_create($date['end'])->modify('+7 hours')->format('d/m/Y');
    $start_date = date_create($date['start'])->modify('+7 hours')->format('d/m/Y');
    $now_date = date_create(date('Y-m-d\TH:i:s'))->modify('+7 hours')->format('d F Y');
    $texts = [
      "Dokumen;: Rekap Penjualan Tiket;\n",
      "Paguyuban;: $company_name;\n",
      "Berdasarkan;: Jenis Tiket;\n",
      "Periode;: $start_date s.d. $end_date;\n\n",
      "No;Jenis Tiket;Harga Tiket;Jumlah Produksi;Jumlah Penumpang;Penerimaan;\n"
    ];
    $footers = [
      "\n\n",
      ";;;;$company_city, $now_date;",
      "\n\n\n\n",
      ";$company_owner;;;$company_finance;\n",
      ";Ketua Paguyuban;;;Bendahara;\n",
    ];
    $res = ReportController::recapSalesByTicketHelper($req, $company_id);
    $total_sale = 0;
    $total_pax = 0;
    $total_ticket = 0;
    foreach ($res as $item) {
      $total_sale += $item['total_sale'];
      $total_pax += $item['total_pax'];
      $total_ticket += $item['total_ticket'];
    }
    $bottom_line = ";Jumlah;;$total_ticket;$total_pax;$total_sale;";
    $settings = $this->downloadSettings($res, $texts, $bottom_line, $footers);
    return response()->streamDownload($settings[0], 'SalesByVenue.csv', $settings[1]);
  }
  public function downloadSalesCombine (Request $req, $company_id) {
    $role_id = $req->user()->role_id;
    $user_company_id = $req->user()->company_id;
    if ($role_id > 2 && $user_company_id != $company_id ) return response()->json('ACCESS_DENIED', 403);
    $company = Company::where('id', $company_id)->first();
    $date = Sorter::dateSorter($req->query('startDate'), $req->query('endDate'));
    $company_name = $company['name'];
    $company_owner = $company['owner'];
    $company_finance = $company['finance'];
    $company_city = $company['city'];
    $end_date = date_create($date['end'])->modify('+7 hours')->format('d/m/Y');
    $start_date = date_create($date['start'])->modify('+7 hours')->format('d/m/Y');
    $now_date = date_create(date('Y-m-d\TH:i:s'))->modify('+7 hours')->format('d F Y');
    $texts = [
      "Dokumen;: Rekap Penjualan Tiket;\n",
      "Paguyuban;: $company_name;\n",
      "Berdasarkan;: Kombinasi Tiket dan Perahu;\n",
      "Periode;: $start_date s.d. $end_date;\n\n",
      "No;Nama Kapal;Jenis Tiket;Harga Tiket;Jumlah Produksi;Jumlah Penumpang;Penerimaan;\n"
    ];
    $footers = [
      "\n\n",
      ";;;;;$company_city, $now_date;",
      "\n\n\n\n",
      ";$company_owner;;;;$company_finance;\n",
      ";Ketua Paguyuban;;;;Bendahara;\n",
    ];
    $res = ReportController::recapSalesCombineHelper($req, $company_id);
    $total_sale = 0;
    $total_pax = 0;
    $total_ticket = 0;
    foreach ($res as $item) {
      $total_sale += $item['total_sale'];
      $total_pax += $item['total_pax'];
      $total_ticket += $item['total_ticket'];
    }
    $bottom_line = ";Jumlah;;;$total_ticket;$total_pax;$total_sale;";
    $settings = $this->downloadSettings($res, $texts, $bottom_line, $footers);
    return response()->streamDownload($settings[0], 'SalesByVenue.csv', $settings[1]);
  }
  public function downloadSalesInstallment (Request $req, $company_id) {
    $role_id = $req->user()->role_id;
    $user_company_id = $req->user()->company_id;
    if ($role_id > 2 && $user_company_id !== $company_id ) return response()->json('ACCESS_DENIED', 403);
    $company = Company::where('id', $company_id)->first();
    $date = Sorter::dateSorter($req->query('startDate'), $req->query('endDate'));
    $company_name = $company['name'];
    $company_owner = $company['owner'];
    $company_finance = $company['finance'];
    $company_city = $company['city'];
    $end_date = date_create($date['end'])->modify('+7 hours')->format('d/m/Y');
    $start_date = date_create($date['start'])->modify('+7 hours')->format('d/m/Y');
    $now_date = date_create(date('Y-m-d\TH:i:s'))->modify('+7 hours')->format('d F Y');
    $texts = [
      "Dokumen;: Rekap Premi Iuran Wajib;\n",
      "Paguyuban;: $company_name;\n",
      "Berdasarkan;: Jenis Perahu;\n",
      "Periode;: $start_date s.d. $end_date;\n\n",
      "No;Nama Kapal;Jumlah Produksi;Jumlah Penumpang;Penerimaan;Premi Iuran Produksi;Premi Iuran Penumpang;\n"
    ];
    $res = ReportController::recapSalesInstallmentHelper($req, $company_id);
    $total_sale = 0;
    $total_pax = 0;
    $total_ticket = 0;
    $total_pax_premium = 0;
    $total_ticket_premium = 0;
    foreach ($res as $item) {
      $total_sale += $item['total_sale'];
      $total_pax += $item['total_pax'];
      $total_ticket += $item['total_ticket'];
      $total_pax_premium =+ $item['total_pax_premium'];
      $total_ticket_premium =+ $item['total_ticket_premium'];
    }
    $bottom_line = ";Jumlah;$total_ticket;$total_pax;$total_sale;$total_ticket_premium;$total_pax_premium;";
    $settings = $this->downloadSettings($res, $texts, $bottom_line);
    return response()->streamDownload($settings[0], 'SalesByVenue.csv', $settings[1]);
  }
}